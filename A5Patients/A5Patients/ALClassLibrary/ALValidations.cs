﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ALClassLibrary
{
    public static class ALValidations
    {
        /// <summary>
        /// Method to capitalize an input string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ALCapitalize(string value)
        {
            value = (value + "").Trim();


            System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower());
            return value;
        }

        /// <summary>
        /// Method to extract numeric digits from string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ALExtractDigits(string value)
        {
            value = (value + "").Trim();
            string digits = string.Empty;

            foreach (Char c in value)
            {
                if (Char.IsDigit(c))
                {
                    digits += c;

                }


            }


                
            return digits;
        }

        /// <summary>
        /// method to verify Canadian postal code
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Boolean ALPostalCodeValidation(string value)
        {
            Regex pattern = new Regex(@"[a-z]\d[a-z] ?\d[a-z]\d");

            Boolean isValid = true;

            if (value != null || pattern.IsMatch(value.ToString()))
            {
                isValid = true;
            }
            else

                isValid = false;
            return isValid;

        }

        


        /// <summary>
        /// method if postal code is valid and it puts it into proper format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ALPostalCodeFormat(string value)
        {
            

            return value;

        }

        /// <summary>
        /// method to verify and format US zip code
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Boolean ALZipCodeValidation( ref string value)
        {
            Boolean isValid = true;

            return isValid;

        }

        public static Boolean ALDateNotInTheFuture(DateTime? date)
        {
            if (date == null) return true;
            DateTime newDate = Convert.ToDateTime(date);
            if (DateTime.Now <= newDate)
                return false;
            else
                return true;
            

        }





    }
}
