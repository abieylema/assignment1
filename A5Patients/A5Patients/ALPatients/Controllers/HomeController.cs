﻿/* ALPatients
 * Version 5
 * Abiey-Lema, 2019.12.01: Created
*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ALPatients.Models;

namespace ALPatients.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            TempData["message"] = "Pharmacy Always Open";
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
