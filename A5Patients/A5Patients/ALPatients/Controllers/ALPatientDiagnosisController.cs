﻿/* ALPatients
 * Version 5
 * Abiey-Lema, 2019.12.01: Created
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ALPatients.Models;
using Microsoft.AspNetCore.Http;

namespace ALPatients.Controllers
{
    public class ALPatientDiagnosisController : Controller
    {
        /* ALPatientDiagnosisController Controls flow of information for the views of the Patient Diagnosis Pages. 
    * Handles URL Requests for the Patient Diagnosis Pages. Gets Data From The Pateints  Database for the 
    * Patient Diagnosis Table using the generated models for Patient Diagnosis Table
    */
        private readonly PatientsContext _context;

        public ALPatientDiagnosisController(PatientsContext context)
        {
            _context = context;
        }

        // Perform Database Access - Get Patient Diagnosis Table From Patients Database and list all records 
        // and returns view to index
        public async Task<IActionResult> Index(int patientId, string patientFirstName, string patientLastName)
        {
            if (patientId != 0)
            {
                HttpContext.Session.SetString(nameof(patientId), patientId.ToString());
                HttpContext.Session.SetString(nameof(patientFirstName), patientFirstName);
                HttpContext.Session.SetString(nameof(patientLastName), patientLastName);
            }

            if (HttpContext.Session.GetString("patientId") != null)
            {
                patientId = Convert.ToInt32(HttpContext.Session.GetString("patientId"));
            }

            else
            {
                TempData["message"] = "Please select a patient id to see the corresponding diagnosis";
                return Redirect("/ALPatient");
            }

            var patientsContext = _context.PatientDiagnosis
                .Where(p => p.Patient.PatientId == patientId)
                .Include(p => p.Diagnosis)
                .Include(p => p.Patient)
                .Include(p => p.PatientTreatment)
                .OrderBy(p => p.Patient.LastName + ", " + p.Patient.FirstName)
                .ThenByDescending(p => p.PatientDiagnosisId);
            return View(await patientsContext.ToListAsync());
        }

        // shows all fields for the selected record 

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patientDiagnosis = await _context.PatientDiagnosis
                .Include(p => p.Diagnosis)
                .Include(p => p.Patient)
                .FirstOrDefaultAsync(m => m.PatientDiagnosisId == id);
            if (patientDiagnosis == null)
            {
                return NotFound();
            }

            return View(patientDiagnosis);
        }

        // Display Blank Input Page and configure drop downs for foreign keys

        public IActionResult Create()
        {
            ViewData["DiagnosisId"] = new SelectList(_context.Diagnosis, "DiagnosisId", "Name");
            ViewData["PatientId"] = new SelectList(_context.Patient, "PatientId", "FirstName");
            return View();
        }

        // Adds new record to the table
        // Differentiated by receiving parameters
        //attribute restricts this to a <form> with action="POST" to enbale only the properties we want to bind to

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PatientDiagnosisId,PatientId,DiagnosisId,Comments")] PatientDiagnosis patientDiagnosis)
        {
            if (ModelState.IsValid)
            {
                _context.Add(patientDiagnosis);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DiagnosisId"] = new SelectList(_context.Diagnosis, "DiagnosisId", "Name", patientDiagnosis.DiagnosisId);
            ViewData["PatientId"] = new SelectList(_context.Patient, "PatientId", "FirstName", patientDiagnosis.PatientId);
            return View(patientDiagnosis);
        }

        // display the selected record for updating

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patientDiagnosis = await _context.PatientDiagnosis.FindAsync(id);
            if (patientDiagnosis == null)
            {
                return NotFound();
            }
            ViewData["DiagnosisId"] = new SelectList(_context.Diagnosis, "DiagnosisId", "Name", patientDiagnosis.DiagnosisId);
            ViewData["PatientId"] = new SelectList(_context.Patient, "PatientId", "FirstName", patientDiagnosis.PatientId);
            return View(patientDiagnosis);
        }

        // requiring HTTP POST, to save the updated record to the table
        //  look for the listed variable names in the flow from the browser and prevent injection

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PatientDiagnosisId,PatientId,DiagnosisId,Comments")] PatientDiagnosis patientDiagnosis)
        {
            if (id != patientDiagnosis.PatientDiagnosisId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patientDiagnosis);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientDiagnosisExists(patientDiagnosis.PatientDiagnosisId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DiagnosisId"] = new SelectList(_context.Diagnosis, "DiagnosisId", "Name", patientDiagnosis.DiagnosisId);
            ViewData["PatientId"] = new SelectList(_context.Patient, "PatientId", "FirstName", patientDiagnosis.PatientId);
            return View(patientDiagnosis);
        }

        // Display the selected record to confirm the delete

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patientDiagnosis = await _context.PatientDiagnosis
                .Include(p => p.Diagnosis)
                .Include(p => p.Patient)
                .FirstOrDefaultAsync(m => m.PatientDiagnosisId == id);
            if (patientDiagnosis == null)
            {
                return NotFound();
            }

            return View(patientDiagnosis);
        }

        // requiring HTTP POST, to delete the record after confirmation

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var patientDiagnosis = await _context.PatientDiagnosis.FindAsync(id);
            _context.PatientDiagnosis.Remove(patientDiagnosis);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PatientDiagnosisExists(int id)
        {
            return _context.PatientDiagnosis.Any(e => e.PatientDiagnosisId == id);
        }
    }
}
