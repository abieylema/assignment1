﻿/* ALPatients
 * Version 5
 * Abiey-Lema, 2019.12.01: Created
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace A5Patients.Controllers
{
    [Authorize(Roles = "administrators")]
    public class ALRoleController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        //Constructor for creating roleManager and userManager objects
        public ALRoleController(UserManager<IdentityUser> userManager,
           RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        // Method for Index Action of ALRoleController 
        //list all current roles, links to manage roles

        public IActionResult Index()
        {
            var
             roles = roleManager.Roles.OrderBy(a => a.Name);
            return
             View(roles);
        }

        //Method with PostBack action to create new roles 
        [HttpPost]
        public async Task<IActionResult> Create(string roleName)
        {
           

            if (string.IsNullOrWhiteSpace(roleName))
            {
                TempData["message"] = "Role Name cannot be empty or blanks";

            }

            else if (await roleManager.RoleExistsAsync(roleName))
            {
                TempData["message"] = "Role name is already on file";

            }

            else
            {






                try
                {


                    IdentityResult identityResult =
                        await roleManager.CreateAsync(new IdentityRole(roleName));
                    if (!identityResult.Succeeded)

                        throw new Exception(identityResult.Errors.FirstOrDefault().Description);
                    TempData["message"] = $"role created: '{roleName}'";




                }
                catch (Exception ex)
                {

                    TempData["message"] = "exception creating role: " + ex.GetBaseException().Message;
                }
            }



            return RedirectToAction(nameof(Index));

        }

        //Method to delete roles 
        
        public async Task<IActionResult> DeleteRole(string roleName)
        {


            if (string.IsNullOrWhiteSpace(roleName))
            {
                TempData["message"] = "Role Name cannot be empty or blanks";

            }

            else if (!await roleManager.RoleExistsAsync(roleName))
            {
                TempData["message"] = "Role name cannot be found";

            }

            else
            {






                try
                {


                    var role =
                        await roleManager.FindByNameAsync(roleName);
                    IdentityResult result = await roleManager.DeleteAsync(role);
                    if (!result.Succeeded)

                        throw new Exception(result.Errors.FirstOrDefault().Description);
                    TempData["message"] = $"role deleted: '{roleName}'";




                }
                catch (Exception ex)
                {

                    TempData["message"] = "exception deleting role: " + ex.GetBaseException().Message;
                }
            }



            return RedirectToAction(nameof(Index));

        }

        //Method with PostBack action to add users to roles 
        [HttpPost]
        public async Task<IActionResult> AddToRoles(string roleName)
        {
            string userName = User.Identity.Name;

            IdentityUser currentUser = await userManager.FindByNameAsync(userName);

            await userManager.AddToRoleAsync(currentUser, roleName);








            return RedirectToAction(nameof(Index));

        }

        
        public async Task<IActionResult> RemoveUserFromRole(string userName, string roleName)
        {
            

            IdentityUser currentUser = await userManager.FindByNameAsync(userName);
            IdentityResult addResult = await userManager.RemoveFromRoleAsync(currentUser, roleName);

            await userManager.RemoveFromRoleAsync(currentUser, roleName);








            return Redirect(nameof(UsersInRole));

        }

        //Method to list all users in a given role

        public async Task<IActionResult> UsersInRole(string roleName)
        {
            if (roleName !=null)
            {
                HttpContext.Session.SetString("roleName", roleName);

            }

            else
            {
                roleName = HttpContext.Session.GetString("roleName"); 

            }
            var usersInRole = await userManager.GetUsersInRoleAsync(roleName);
            var allUsers = userManager.Users;
            List<IdentityUser> userNotInRole = new List<IdentityUser>();

            foreach (var item in allUsers)
            {
                if (!usersInRole.Contains(item))
                {
                    userNotInRole.Add(item);
                    
                }

            }



            ViewData["notInRole"] = new SelectList(userNotInRole,  "UserName");
            

            return View(usersInRole);
        }

        //Method to add users to a role
        public async Task<IActionResult> AddUserToRole(string userName)
        {
            string roleName = HttpContext.Session.GetString("roleName");
            var user = await userManager.FindByNameAsync(userName);

            try
            {
                IdentityResult result = await userManager.AddToRoleAsync(user, roleName);
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);

                }

                TempData["message"] = $"User added to role";
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.GetBaseException().Message;

                
            }




            return RedirectToAction("usersInRole");



        }
    }
}