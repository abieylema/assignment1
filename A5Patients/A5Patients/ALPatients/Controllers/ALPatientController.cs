﻿/* ALPatients
 * Version 5
 * Abiey-Lema, 2019.12.01: Created
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ALPatients.Models;
using ALClassLibrary;

namespace ALPatients.Controllers
{
    /* ALPatientController Controls flow of information for the views of the Patient Pages. 
    * Handles URL Requests for the Patient Pages. Gets Data From The Pateints  Database for the 
    * Patient Table using the generated models for Patient Table
    */
    public class ALPatientController : Controller
    {
        private readonly PatientsContext _context;

        public ALPatientController(PatientsContext context)
        {
            _context = context;
        }

        // Perform Database Access - Get Patient Table From Patients Database and list all records
        //and returns view to index

        public async Task<IActionResult> Index()
        {
            
            var patientsContext = _context.Patient
                .OrderBy(a=> a.LastName)
                .ThenBy(a=> a.FirstName)
                .Include(p => p.ProvinceCodeNavigation);
            return View(await patientsContext.ToListAsync());
        }

        // shows all fields for the selected record

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patient
                .Include(p => p.ProvinceCodeNavigation)
                .FirstOrDefaultAsync(m => m.PatientId == id);
            if (patient == null)
            {
                return NotFound();
            }

            return View(patient);
        }

        // Display Blank Input Page to add new Patient and configure drop downs for foreign keys

        public IActionResult Create()
        {
            ViewData["ProvinceCode"] = new SelectList(_context.Province, "ProvinceCode", "ProvinceCode");
            return View();
        }

        // Adds new record to the table
        // Differentiated by receiving parameters
        //attribute restricts this to a <form> with action="POST" to enbale only the properties we want to bind to

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PatientId,FirstName,LastName,Address,City,ProvinceCode,PostalCode,Ohip,DateOfBirth,Deceased,DateOfDeath,HomePhone,Gender")] Patient patient)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(patient);
                    await _context.SaveChangesAsync();
                    if (TempData != null) TempData["message"] = "patient updated";
                    return RedirectToAction(nameof(Index));
                }

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("exception on update is: ", ex.GetBaseException().Message);
            }
            


            ViewData["ProvinceCode"] = new SelectList(_context.Province, "ProvinceCode", "ProvinceCode", patient.ProvinceCode);
            
            return View(patient);


           

           
        }

        // display the selected record for updating

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patient.FindAsync(id);
            if (patient == null)
            {

                return NotFound();
            }
            ViewData["ProvinceCode"] = new SelectList(_context.Province, "ProvinceCode", "ProvinceCode", patient.ProvinceCode);
            return View(patient);
        }

        // requiring HTTP POST, to save the updated record to the table
        //  look for the listed variable names in the flow from the browser and prevent injection

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PatientId,FirstName,LastName,Address,City,ProvinceCode,PostalCode,Ohip,DateOfBirth,Deceased,DateOfDeath,HomePhone,Gender")] Patient patient)
        {
            if (id != patient.PatientId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patient);
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    if (!PatientExists(patient.PatientId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        ModelState.AddModelError("exception on update is: ", ex.GetBaseException().Message);
                        
                    }
                }
                if (TempData != null) TempData["message"] = "patient updated";
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProvinceCode"] = new SelectList(_context.Province, "ProvinceCode", "ProvinceCode", patient.ProvinceCode);
            return View(patient);
        }

        // Display the selected record to confirm the delete

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patient
                .Include(p => p.ProvinceCodeNavigation)
                .FirstOrDefaultAsync(m => m.PatientId == id);
            if (patient == null)
            {
                return NotFound();
            }

            return View(patient);
        }

        // requiring HTTP POST, to delete the record after confirmation

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var patient = await _context.Patient.FindAsync(id);
            _context.Patient.Remove(patient);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PatientExists(int id)
        {
            return _context.Patient.Any(e => e.PatientId == id);
        }
    
}
}
