﻿using ALClassLibrary;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ALPatients.Models
{
    [ModelMetadataType(typeof(ALPatientMetadata))]
    public partial class Patient : IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            PatientsContext _context = new PatientsContext();
            

            FirstName = ALValidations.ALCapitalize(FirstName);
            if (FirstName =="")
            {
                yield return new ValidationResult("first name is required",
                    new[] { nameof(FirstName) });
            }
            LastName = ALValidations.ALCapitalize(LastName);
            if (LastName == "")
            {
                yield return new ValidationResult("last name is required",
                    new[] { nameof(LastName) });
            }
            
            Address = ALValidations.ALCapitalize(Address);

            City = ALValidations.ALCapitalize(City);

            if (PostalCode != null && !ALValidations.ALPostalCodeValidation(PostalCode))
            {

                yield return new ValidationResult("is not a valid Cdn postal pattern: A3A 3A3",
                new[] { nameof(PostalCode) });



            }
            if (ProvinceCode != "" && ProvinceCode != null)
            {
                ProvinceCode = (ProvinceCode + "").Trim().ToUpper();
                Province province = null;
                string errorMsg = "";
                try
                {
                    province = _context.Province
                        .FirstOrDefault(a => a.ProvinceCode == ProvinceCode);

                }
                catch (Exception ex)
                {

                    errorMsg = "error retreiving province code: "
                         + ex.GetBaseException().Message;
                }

                if (province == null)
                {
                    yield return new ValidationResult("province code is not on file",
                    new[] { nameof(ProvinceCode) });


                }

                if (errorMsg != "")
                {
                    yield return new ValidationResult("province code is not on file",
                    new[] { nameof(ProvinceCode) });

                }

            }


            if (!ALValidations.ALDateNotInTheFuture((DateTime?)DateOfBirth))
            {
               
                    yield return new ValidationResult("Date Can't Be In The Future",
                    new[] { nameof(DateOfBirth) });

                

            }
            

            Gender = ALValidations.ALCapitalize(Gender);
            if (Gender == "")
            {
                yield return new ValidationResult("gender is required",
                    new[] { nameof(Gender) });
            }














            yield return ValidationResult.Success;
        }
    }
    public class ALPatientMetadata
    {
        public int PatientId { get; set; }
       
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
       
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        
        [Display(Name = "Street Address")]
        public string Address { get; set; }
        public string City { get; set; }
        [Display(Name = "Province Code")]
        public string ProvinceCode { get; set; }
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }
        [Display(Name = "OHIP")]
        
        public string Ohip { get; set; }
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? DateOfBirth { get; set; }
        public bool Deceased { get; set; }
        [Display(Name = "Date of Death")]
        [DataType(DataType.Date)]
        public DateTime? DateOfDeath { get; set; }
        [RegularExpression(@"^\d{3}-\d{3}-\d{4}$")]
        [Display(Name = "Home Phone")]
        public string HomePhone { get; set; }
        
        public string Gender { get; set; }
    }
}
